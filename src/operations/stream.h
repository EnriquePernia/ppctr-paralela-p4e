#ifndef FLAME_OPERATIONS_STREAM_H
#define FLAME_OPERATIONS_STREAM_H

#include <cstring>

#include <cstdint>
#include <iostream>
#include <string>

#include <fstream>
#include <iomanip>
#include <sstream>

void
stream_init(char* buffer);

int
stream_parse(char* buffer);

unsigned int
stream_init_response(char* buffer);

char*
stream_parse_response(char* buffer);

class OperationStream
{
public:
  OperationStream();
  void update(char* ptr);
  void update(const std::string& s);
  void update(std::istream& is);
  std::string final();

private:
  uint32_t dig[5];
  std::string buffer;
  uint64_t transforms;
};

#endif // FLAME_OPERATIONS_STREAM_H
