#include "client.h"

/**
 * @private
 **/
Status
client_create(Client* client, Config* config)
{
  Status st = Status::Ok;

  int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1) {
    perror("[error] socket");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] socket");
  }

  client->addr.sin_addr.s_addr = inet_addr(config->host);
  client->addr.sin_family = AF_INET;
  client->addr.sin_port = htons(config->port);

  client->timeout.tv_sec = 3;
  client->timeout.tv_usec = 0;

  if (setsockopt(
        socket_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  if (setsockopt(
        socket_fd, SOL_SOCKET, SO_SNDTIMEO, (char*)&client->timeout, sizeof(client->timeout)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  client->socket = socket_fd;

  return st;
}

/**
 * @private
 **/
Status
client_reconfig_echo(Client* client, Config* config)
{
  Status st = Status::Ok;

  client->timeout.tv_sec = 0;
  client->timeout.tv_usec = 700000;

  if (setsockopt(
        client->socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&client->timeout, sizeof(client->timeout)) <
      0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  return st;
}

/**
 * @private
 **/
Status
client_connect(Client* client, Config* config)
{
  if (connect(client->socket, (const sockaddr*)&client->addr, sizeof(client->addr)) < 0) {
    perror("[error] connect");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] connect");
  }
  return Status::Ok;
}

/**
 * @private
 **/
Status
client_disconnect(Client* client, Config* config)
{
  close(client->socket);
  if (config->debug) {
    puts("[done] disconnect");
  }
  return Status::Ok;
}

Status
client(Config* config)
{
  Status st = Status::Ok;
  Client client;

  char buffer[1000];
  char message[1000], server_reply[2000];

  Protocol protocol = config->protocol;
  Operation operation = operation_from_protocol(protocol);

  bool cont = true;
  int times = 0;
  int max = MAX_REQS;

  while (cont) {
    st = client_create(&client, config);
    if (st != Status::Ok) {
      return st;
    }

    st = client_connect(&client, config);
    if (st != Status::Ok) {
      return st;
    }

    if (operation == Operation::Echo) {
      st = client_reconfig_echo(&client, config);
      if (st != Status::Ok) {
        return st;
      }

      long init_value = 111;
      echo_init(buffer, init_value);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      char* ptr = (char*)&server_reply;

      int expected_size = 5 + 40 * sizeof(float); // 5 header
      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          if (errno != ETIMEDOUT && errno != EAGAIN) {
            puts("[error] recv");
            return Status::Err;
          }
          if (config->debug) {
            puts("[info] recv timeout");
          }
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      long value = echo_parse_response(server_reply);
      if (value == init_value + 1) {
        if (config->debug) {
          printf("====recv==== [%dB]\n", size);
          printf("%ld", value);
          printf("%s============\n", (size > 0 ? "\n" : ""));
        }
      } else {
        if (config->debug) {
          printf("[echo] wrong response: %d\n", value);
        }
      }
    } else if (operation == Operation::MatMul) {
      unsigned int dim = 3 % MAX_DIM;
      matmul_init(buffer, dim);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      char* ptr = (char*)&server_reply;

      int expected_size = 8 + dim * dim * sizeof(float); // 8 header
      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          puts("[error] recv");
          return Status::Err;
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      if (config->debug) {
        printf("====recv==== [%dB %d chunk(s)] matmul: C = A*B:\n", size, chunks);
        float* C = matmul_parse_response(server_reply);
        if (C == nullptr) {
          puts("[error] matmul response");
        } else {
          matmul_matrix_print(C, dim);
        }
        printf("============\n");
      }
    } else if (operation == Operation::Stream) {
      stream_init(buffer);

      if (send(client.socket, buffer, strlen(buffer) + 1, 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", buffer);
      }

      const char* message1 =
        "El ecologismo (en ocasiones llamado el movimiento verde o ambientalista) ";
      const char* message2 = "es un variado movimiento político, social y global, que defiende la "
                             "protección del medio ambiente. ";
      const char* message3 =
        "Recomiendo la película 'Barbacana, la huella del lobo', recientemente proyectada en cines "
        "y rodada durante 12 años. Refleja el conflicto por la conservación del lobo ibérico y "
        "pone de manifiesto la ineficacia de las medidas actuales.";

      if (send(client.socket, message1, strlen(message1), 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message1);
      }
      if (send(client.socket, message2, strlen(message2), 0) < 0) {
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message2);
      }
      if (send(client.socket, message3, strlen(message3) + 1, 0) < 0) { // \0 sent
        puts("[error] send");
        return Status::Err;
      }
      if (config->debug) {
        printf("[done] send: '%s'\n", message3);
      }

      char* ptr = (char*)&server_reply;

      int expected_size = 8 + 40 * sizeof(char); // 8 header
      int size = 0;
      int chunks = 0;
      int read_size = 0;
      do {
        ptr = (ptr + read_size);
        read_size = recv(client.socket, ptr, 2000, 0);
        if (read_size < 0) {
          puts("[error] recv");
          return Status::Err;
        } else {
          chunks++;
          size += read_size;
        }
      } while (read_size > 0 && size < expected_size);
      if (config->debug) {
        printf("====recv==== [%dB %d chunk(s)] stream: B = fx(A):\n", size, chunks);
        ptr = stream_parse_response(server_reply);
        puts(ptr);
        printf("============\n");
      }

    } else {
      puts("[cmd] invalid operation");
    }

    st = client_disconnect(&client, config);
    if (st != Status::Ok) {
      return st;
    }

    times++;
    if (times >= max) {
      cont = false;
    }
  }
  if (config->debug) {
    puts("[exit]");
  }

  return Status::Ok;
}