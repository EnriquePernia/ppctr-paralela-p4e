#include "server.h"

/**
 * @private
 **/
Status
server_create(Server* server, Config* config)
{
  Status st = Status::Ok;

  server->addr.sin_family = AF_INET;
  server->addr.sin_addr.s_addr = inet_addr(config->host);
  server->addr.sin_port = htons(config->port);

  int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (socket_fd == -1) {
    perror("[error] socket");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] socket");
  }

  if (bind(socket_fd, (const sockaddr*)&(server->addr), sizeof(server->addr)) < 0) {
    perror("[error] bind");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] bind");
  }

  server->socket = socket_fd;

  return st;
}

/**
 * @private
 **/
Status
server_listen(Server* server, Config* config)
{
  if (listen(server->socket, 3) != 0) {
    perror("[error] listen");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] listen");
  }
  return Status::Ok;
}

/**
 * @private
 **/
Status
server_accept(Server* server, Config* config)
{
  if (config->debug) {
    puts("[waiting] accept");
  }
  int c = sizeof(struct sockaddr_in);
  int socket_fd = accept(server->socket, (struct sockaddr*)&server->client_addr, (socklen_t*)&c);
  if (socket_fd < 0) {
    perror("[error] accept");
    return Status::Err;
  }
  if (config->debug) {
    puts("[done] accept");
  }

  int reuse = 1;

  if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) < 0) {
    perror("[error] setsockopt");
    return Status::Err;
  }

  server->client_socket = socket_fd;

  return Status::Ok;
}

Status
server(Config* config)
{
  Status st = Status::Ok;
  Server server;

  st = server_create(&server, config);
  if (st != Status::Ok) {
    return st;
  }

  st = server_listen(&server, config);
  if (st != Status::Ok) {
    return st;
  }

  Protocol protocol = config->protocol;
  Operation operation = operation_from_protocol(protocol);

  bool cont = true;
  int times = 0;
  int max = MAX_REQS;
  max = 3;
  while (cont) {

    st = server_accept(&server, config);
    if (st != Status::Ok) {
      return st;
    }

    int read_size;
    char *buffer, message[2000], client_message[2000];

    if (config->debug) {
      puts("[waiting] recv");
    }
    while ((read_size = recv(server.client_socket, client_message, 2000, 0)) > 0) {
      if (config->debug) {
        printf("====recv==== [%dB]\n", read_size);
        printf("%s", client_message);
        printf("%s============\n", (read_size > 0 ? "\n" : ""));
      }

      buffer = (char*)&client_message;

      if (strncmp("op:", client_message, 3) != 0) {
        puts("[cmd] invalid");
        continue;
      }

      Operation req_op = match_operation(operation, buffer);

      if (req_op == Operation::Echo) {
        if (config->debug) {
          puts("[cmd] echo");
        }
        long val = echo_parse(buffer);
        if (config->debug) {
          printf("[echo] parsed value: '%ld'\n", val);
        }
        echo_init_response(buffer, val);
        if (config->debug) {
          printf("[echo] send message: '%s'\n", buffer);
        }
        write(server.client_socket, buffer, strlen(buffer) + 1);
      } else if (req_op == Operation::MatMul) {
        if (config->debug) {
          puts("[cmd] matmul");
        }
        unsigned int dim = matmul_parse(buffer) % MAX_DIM;
        if (config->debug) {
          printf("[matmul] parsed dim: '%d'\n", dim);
        }
        float** Ms = matmul_alloc(dim);
        if (config->debug) {
          printf("[matmul] filling and computing\n");
        }
        matmul_fill(Ms, dim);
        matmul_compute(Ms, dim);
        float* C = Ms[2];
        int size = dim * dim * sizeof(float);
        unsigned int lbuffer = matmul_init_response(buffer);
        if (config->debug) {
          printf("A:\n");
          matmul_matrix_print(Ms[0], dim);
          printf("B:\n");
          matmul_matrix_print(Ms[1], dim);
          printf("C:\n");
          matmul_matrix_print(C, dim);
        }
        write(server.client_socket, buffer, lbuffer);
        write(server.client_socket, (char*)C, size);
        matmul_destroy(Ms);
      } else if (req_op == Operation::Stream) {
        if (config->debug) {
          puts("[cmd] stream");
        }
        OperationStream opstream;
        int offset = stream_parse(buffer);
        char* ptr = &client_message[offset];

        read_size -= offset;
        int end;
        bool expect_more = (read_size == 0);
        do {
          end = strnlen(ptr, read_size);
          if (config->debug) {
            printf("[stream] chunk: '");
            fwrite(ptr, 1, end, stdout);
            printf("' [%dB]\n", end);
          }

          if (end > 0) {
            std::string chunk(ptr, end);
            opstream.update(chunk);
            if (config->debug) {
              printf("[stream] chunk updated [%dB]\n", end);
            }
          }

          expect_more = (read_size == 0 || end == read_size); // no end yet
          if (expect_more) {
            if (config->debug) {
              printf("[stream] waiting\n");
            }
            read_size = recv(server.client_socket, client_message, 2000, 0);
            offset = 0;
            ptr = client_message;
            if (read_size <= 0) {
              expect_more = false;
            }
          }

        } while (expect_more);
        // streaming: inner value is consumed afterwards
        const char* streamed = strdup(opstream.final().c_str());
        int lstreamed = strlen(streamed);
        if (config->debug) {
          printf("[stream] streamed: '%s'\n", streamed);
        }
        unsigned int lbuffer = stream_init_response(buffer);
        write(server.client_socket, buffer, lbuffer);
        write(server.client_socket, streamed, lstreamed + 1);
      } else {
        puts("[cmd] invalid");
        continue;
      }
    }

    if (read_size == 0) {
      puts("[info] client disconnected");
      fflush(stdout);
    } else if (read_size == -1) {
      perror("[error] recv");
    }

    times++;
    if (times >= max) {
      cont = false;
    }
  }
  if (config->debug) {
    puts("[exit]");
  }

  return Status::Ok;
}
